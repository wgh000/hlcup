package globals

import (
	"go.uber.org/zap"
)

var GlobalTimestamp int32

var Logger, _ = zap.NewProduction()
