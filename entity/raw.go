package entity

import (
	"gitlab.com/dennis.khassan/hlcup/globals"
	"hash/crc32"
)

type AccountsRaw struct {
	Accounts []AccountRaw `json:"accounts"`
}

type AccountRaw struct {
	Id         int32       `json:"id"`
	Email      string      `json:"email"`
	FirstName  string      `json:"fname"`
	SecondName string      `json:"sname"`
	Phone      string      `json:"phone"`
	Sex        string      `json:"sex"`
	Birth      int32       `json:"birth"`
	Country    string      `json:"country"`
	City       string      `json:"city"`
	Joined     uint32      `json:"joined"`
	Status     string      `json:"status"`
	Interests  []string    `json:"interests"`
	Premium    *PremiumRaw `json:"premium"`
	Likes      []*LikeRaw  `json:"likes"`
}

type PremiumRaw struct {
	Start  int32 `json:"start"`
	Finish int32 `json:"finish"`
}

type LikeRaw struct {
	Id        int32 `json:"id"`
	Timestamp int32 `json:"ts"`
}

func (raw *AccountRaw) FetchDataEntities() (*Account, []*Interest, []*Like) {
	rawSex := false
	if raw.Sex == "m" {
		rawSex = true
	}
	rawStatus := uint8(0)
	switch raw.Status {
	case "свободны":
		rawStatus = 1
		break
	case "заняты":
		rawStatus = 2
		break
	case "всё сложно":
		rawStatus = 3
		break
	}

	isPremium := false
	if raw.Premium != nil &&
		raw.Premium.Finish != 0 &&
		globals.GlobalTimestamp > raw.Premium.Start &&
		globals.GlobalTimestamp < raw.Premium.Finish {

		isPremium = true
	}

	interests := make([]*Interest, 0, len(raw.Interests))
	rawInterestIds := make([]uint32, 0, len(raw.Interests))

	for _, interestString := range raw.Interests {
		interestId := crc32.ChecksumIEEE([]byte(interestString))

		interests = append(interests, &Interest{
			Id:   interestId,
			Name: interestString,
		})

		rawInterestIds = append(rawInterestIds, interestId)
	}

	likes := make([]*Like, 0, len(raw.Likes))
	for _, like := range raw.Likes {
		likes = append(likes, &Like{
			AccountFromId: raw.Id,
			AccountToId: like.Id,
			Timestamp: like.Timestamp,
		})
	}

	return &Account{
		Id:          raw.Id,
		Email:       raw.Email,
		FirstName:   raw.FirstName,
		SecondName:  raw.SecondName,
		Phone:       raw.Phone,
		Sex:         rawSex,
		Birth:       raw.Birth,
		Country:     raw.Country,
		City:        raw.City,
		Joined:      raw.Joined,
		Status:      rawStatus,
		InterestIds: rawInterestIds,
		Premium:     isPremium,
	}, interests, likes
}
