package entity

type Like struct {
	AccountFromId int32 `reindex:"acc_from_id"`
	AccountToId   int32 `reindex:"acc_to_id"`
	AccountFrom   *Account `reindex:"acc_from,,joined"`
	AccountTo     *Account `reindex:"acc_to,,joined"`
	Timestamp     int32   `reindex:"timestamp"`
}

func (like *Like) Join(field string, subitems []interface{}, context interface{}) {
	switch field {
	case "acc_from":
		for _, joinItem := range subitems {
			like.AccountFrom = joinItem.(*Account)
		}
	case "acc_to":
		for _, joinItem := range subitems {
			like.AccountFrom = joinItem.(*Account)
		}
	}
}
