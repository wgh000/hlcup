package entity

type Interest struct {
	Id       uint32     `reindex:"id,pk"`
	Name     string     `reindex:"name"`

	Accounts []*Account `reindex:"accounts,,joined"`
}

func (interest *Interest) Join(field string, subitems []interface{}, context interface{}) {
	switch field {
	case "accounts":
		for _, joinItem := range subitems {
			interest.Accounts = append(interest.Accounts, joinItem.(*Account))
		}
	}
}
